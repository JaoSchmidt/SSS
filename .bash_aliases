alias sl=ls
alias cat='bat --paging=never'
alias less='bat --paging=always'
alias 'cd..'='cd_up'
alias trr='transmission-remote'
