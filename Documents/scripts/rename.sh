#!/bin/bash
for i in *.rar; do
  first_folder=$(unrar lb "$i" | head -n 1 | cut -d/ -f1 )
  counter=1

  file_name=$first_folder
	while [ -e "$file_name.rar" ]; do
    file_name="$first_folder.$counter"
		counter=$((counter+1))
	done
  # Rename the rar file with the name of the first folder
  mv -n "$i" "$file_name.rar"
done;
