#!/bin/sh

BLANK="$(xrdb -query | grep '*.background' | cut -f 2)"
CLEAR="$(xrdb -query | grep '*.background' | cut -f 2)"
DEFAULT="$(xrdb -query | grep '*.color4' | cut -f 2)"
TEXT="$(xrdb -query | grep '*.color6' | cut -f 2)"
WRONG="$(xrdb -query | grep '*.color1' | cut -f 2)"
VERIFYING="$(xrdb -query | grep '*.color3' | cut -f 2)"

i3lock \
--insidever-color=$CLEAR     \
--ringver-color=$VERIFYING   \
\
--insidewrong-color=$CLEAR   \
--ringwrong-color=$WRONG     \
\
--inside-color=$BLANK        \
--ring-color=$DEFAULT        \
--line-color=$BLANK          \
--separator-color=$DEFAULT   \
\
--verif-color=$TEXT          \
--wrong-color=$TEXT          \
--time-color=$TEXT           \
--date-color=$TEXT           \
--layout-color=$TEXT         \
--keyhl-color=$WRONG         \
--bshl-color=$WRONG          \
\
--screen 1                   \
--blur 5                     \
--clock                      \
--indicator                  \
--time-str="%H:%M:%S"        \
--date-str="%A, %Y-%m-%d"       \
--keylayout 1                \

