require("nvim-tree").setup({
  sort_by = "case_sensitive",
  disable_netrw = true,
  view = {
	 side = "left",
	 centralize_selection = true,
	 signcolumn = "no",
    mappings = {
      list = {
        { key = "u", action = "dir_up" },
      },
    },
  },
  renderer = {
    group_empty = true,
	 indent_markers = {
		enable=true,
		inline_arrows = false,
	 }
  },
  filters = {
    dotfiles = false,
	 custom = {
		 "^.git$"
	 }
  },
  diagnostics = {
	  enable=true
  },
  update_focused_file = {
	  enable = true,
	  update_cwd = true
  },
	actions = {
		change_dir ={
			global = true,
		}
	}
})
vim.api.nvim_set_keymap('n','<C-b>',':NvimTreeToggle<CR>',{noremap = true, silent = true})
