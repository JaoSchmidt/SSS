call plug#begin('~/.config/nvim/plugged')

Plug 'kaicataldo/material.vim', { 'branch': 'main' } " Tema usado no vim

Plug 'mg979/vim-visual-multi', {'branch': 'master'} " Ferramenta para escrever em multiplas linhas com o ctrl+seta
" Plug 'andweeb/presence.nvim' " Ferramenta que permite usar o discord presence
Plug 'lambdalisue/suda.vim' " Ferramenta que permite usar o sudo para salvar arquivos protegidos

Plug 'nvim-lualine/lualine.nvim' " Permite o uso da linha bonitinha

Plug 'kyazdani42/nvim-web-devicons' " Permite o uso de alguns icones especiais
Plug 'kyazdani42/nvim-tree.lua' " Adiciona a ávore de aqruivos
Plug 'romgrk/barbar.nvim' " Adiciona os guias, usa o nvim-web-devicons também

Plug 'nvim-lua/plenary.nvim' " Requerimento para o telescope
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' } " Procura as paradas

Plug 'nvim-treesitter/nvim-treesitter'
Plug 'windwp/nvim-autopairs'

Plug 'tibabit/vim-templates'

call plug#end()

lua require('nvim-tree-config')
lua require('lualine-config')
lua require('nvim-treesitter-config')
lua require("nvim-autopairs").setup {}
" lua require('nvim-autotag-config')


" abre esse arquivo usando o \rc
:nnoremap <leader>rc :vsplit $MYVIMRC<cr>

" para o vim:
"command SW :execute ':w !sudo tee > /dev/null %' | :edit!
" para o neovim:
command! SW SudaWrite

set foldmethod="marks"
" evitar o shift quando detecta um erro
set signcolumn=yes
set showcmd " mostra a linha de comando
set showmatch " marca o parentesis par
set smartcase " ignora maiusculas apenas quando não há maiusculas
" set autowrite " 
set mouse=a " permite uso do mouse
" set nocompatible 
set backspace=2 " permite uso do backspace no insert mode
map <leader><space> :let @/=''<cr> 

" permite numeros hibridos
set number
set relativenumber

" alias :Wq = :wq
command! Wq wq
command! W w
command! WQ wq
command! Q q

" simplifica indexação
set tabstop=3
set shiftwidth=3
set softtabstop=3
set autoindent
set smartindent

" list chars
set list lcs=tab:┆\ 

set matchpairs+=<:>
set ttyfast

set laststatus=2
set encoding=utf-8

" theme
" syntax on
set termguicolors
let g:material_terminal_italics = 1
let g:material_theme_style = 'ocean'
colorscheme material

set hlsearch

" Permite o uso de skeletons
" augroup templates
" 	au!
" 	autocmd BufNewFile *.* silent! execute '0r $HOME/.config/nvim/templates/skel.'.expand("<afile>:e")
" augroup END

" ------------------------------------------------------------------------------------------ "
" Funções para o set paste autoToggle
" ------------------------------------------------------------------------------------------ "

let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
  set pastetoggle=<Esc>[201~
  set paste
  return ""
endfunction

" ------------------------------------------------------------------------------------------ "
" vim-template config
" ------------------------------------------------------------------------------------------ "

let g:tmpl_search_paths = ['~/.config/nvim/templates']


" ------------------------------------------------------------------------------------------ "
" telescope config
" ------------------------------------------------------------------------------------------ "

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr


" ------------------------------------------------------------------------------------------ "
" barbar
" ------------------------------------------------------------------------------------------ "

" Move to previous/next
nnoremap <silent>    <A-,> <Cmd>BufferPrevious<CR>
nnoremap <silent>    <A-.> <Cmd>BufferNext<CR>

" Re-order to previous/next
nnoremap <silent>    <A-<> <Cmd>BufferMovePrevious<CR>
nnoremap <silent>    <A->> <Cmd>BufferMoveNext<CR>

lua << EOF
require'bufferline'.setup {
  hide = {extensions = false, inactive = false}
}
EOF
