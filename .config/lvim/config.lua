vim.opt.relativenumber = true
vim.opt.scrolloff = 4

vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = false

vim.wo.list = true
vim.wo.listchars = "tab:▸ ,trail:·"

vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
-- vim.opt.foldtext = "nvim_treesitter#foldtext()"

lvim.plugins = {
	{ "lambdalisue/suda.vim" },
	{ "kaicataldo/material.vim", branch = "main" },
	{ "mg979/vim-visual-multi", branch = "master" },
	{
		"Badhi/nvim-treesitter-cpp-tools",
	},
	{
		"folke/todo-comments.nvim",
		dependences = { "nvim-lua/plenary.nvim" },
		opts = {},
	},
	{
		"iamcco/markdown-preview.nvim",
		cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
		ft = { "markdown" },
		build = function()
			vim.fn["mkdp#util#install"]()
		end,
	},
	{
		"andweeb/presence.nvim",
		config = function()
			require("presence").setup({
				blacklist = { "dol", "firemod-dol", "firemod-release" },
				show_time = false,
			})
		end,
	},
	{
		"folke/persistence.nvim", -- allow to save buffer sessions
		event = "BufReadPre",
		config = function()
			require("persistence").setup({
				dir = vim.fn.expand(vim.fn.stdpath("state") .. "/sessions/"),
				options = { "buffers", "curdir", "tabpages", "winsize" },
			})
		end,
	},
	{
		"azratul/live-share.nvim",
		dependencies = {
			"jbyuki/instant.nvim",
		},
	},
	-- { "AbaoFromCUG/websocket.nvim" },
	-- {
	-- 	"SUSTech-data/neopyter",
	-- 	---@type neopyter.Option
	-- 	opts = {
	-- 		mode = "direct",
	-- 		remote_address = "127.0.0.1:9001",
	-- 		file_pattern = { "*.ju.*" },
	-- 		on_attach = function(bufnr)
	-- 			-- do some buffer keymap
	-- 		end,
	-- 		highlight = {
	-- 			enable = true,
	-- 			shortsighted = false,
	-- 		},
	-- 		parser = {
	-- 			-- trim leading/tailing whitespace of cell
	-- 			trim_whitespace = false,
	-- 		},
	-- 	},
	-- },
}

-- ###################################################### --
-- general stuff
-- ###################################################### --

vim.cmd("command! SW SudaWrite") -- allow write on root files with a password

-- command aliases
vim.cmd("command! Wq wq")
vim.cmd("command! WQ WQ")
vim.cmd("command! W w")
vim.cmd("command! Q q")

-- vim.api.nvim_set_keymap("i", "<C-BS>", "<C-W>", { noremap = true })
vim.api.nvim_set_keymap("n", "<S-L>", ":bnext<CR>", { noremap = true, silent = true }) -- go to previous buffer using shift+l
vim.api.nvim_set_keymap("n", "<S-H>", ":bprevious<CR>", { noremap = true, silent = true }) -- go to next buffer using shift+h
lvim.colorscheme = "material"
vim.g.material_theme_style = "ocean"
lvim.transparent_window = true
vim.g.material_terminal_italics = 1
-- vim.b.editorconfig = true -- use this do enable/disable .editorconfig file

lvim.builtin.nvimtree.setup = {
	sort_by = "case_sensitive",
	disable_netrw = true,
	view = {
		side = "left",
		signcolumn = "no",
	},
	renderer = {
		group_empty = true,
		indent_markers = {
			enable = true,
			inline_arrows = false,
		},
	},
	filters = {
		dotfiles = false,
		custom = { "^.git$" },
	},
	diagnostics = {
		enable = true,
	},
	update_focused_file = {
		enable = false,
		update_root = false,
	},
}

-- coqtail configuration
-- lvim.keys.normal_mode["<leader>bc"] = ":bd<CR>"
-- lvim.builtin.which_key.mappings['c'] = {}
-- lvim.keys.normal_mode["<leader-c>"] = false

--[[]
lvim is the global options object

Linters should be
filled in as strings with either
a global executable or a path to
an executable
]]
-- THESE ARE EXAMPLE CONFIGS FEEL FREE TO CHANGE TO WHATEVER YOU WANT

lvim.log.level = "warn"
lvim.format_on_save.enabled = true
-- lvim.colorscheme = "lunar"
-- to disable icons and use a minimalist setup, uncomment the following
-- lvim.use_icons = false

-- keymappings [view all the defaults by pressing <leader>Lk]
lvim.leader = "space"
-- add your own keymapping
lvim.keys.normal_mode["<C-s>"] = ":w<cr>"
lvim.keys.normal_mode["<S-l>"] = ":BufferLineCycleNext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":BufferLineCyclePrev<CR>"
-- unmap a default keymapping
-- vim.keymap.del("n", "<C-Up>")
-- override a default keymapping
-- lvim.keys.normal_mode["<C-q>"] = ":q<cr>" -- or vim.keymap.set("n", "<C-q>", ":q<cr>" )

-- Change Telescope navigation to use j and k for navigation and n and p for history in both input and normal mode.
-- we use protected-mode (pcall) just in case the plugin wasn't loaded yet.
-- local _, actions = pcall(require, "telescope.actions")
-- lvim.builtin.telescope.defaults.mappings = {
--   -- for input mode
--   i = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--     ["<C-n>"] = actions.cycle_history_next,
--     ["<C-p>"] = actions.cycle_history_prev,
--   },
--   -- for normal mode
--   n = {
--     ["<C-j>"] = actions.move_selection_next,
--     ["<C-k>"] = actions.move_selection_previous,
--   },
-- }

-- map telescope commands to simple aliases
vim.cmd("cabbrev ff Telescope find_files")
vim.cmd("cabbrev fg Telescope live_grep")
vim.cmd("cabbrev fb Telescope buffers")
vim.cmd("cabbrev fh Telescope help_tags")

-- Change theme settings
lvim.builtin.telescope.theme = "center"
-- lvim.builtin.theme.options.dim_inactive = true
-- lvim.builtin.theme.options.style = "storm"

-- Use which-key to add extra bindings with the leader-key prefix
-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["t"] = {
	name = "+Trouble",
	r = { "<cmd>Trouble lsp_references<cr>", "References" },
	f = { "<cmd>Trouble lsp_definitions<cr>", "Definitions" },
	d = { "<cmd>Trouble document_diagnostics<cr>", "Diagnostics" },
	q = { "<cmd>Trouble quickfix<cr>", "QuickFix" },
	l = { "<cmd>Trouble loclist<cr>", "LocationList" },
	w = { "<cmd>Trouble workspace_diagnostics<cr>", "Workspace Diagnostics" },
}

-- TODO: User Config for predefined plugins
-- After changing plugin config exit and reopen LunarVim, Run :PackerInstall :PackerCompile
lvim.builtin.alpha.active = true
lvim.builtin.alpha.mode = "dashboard"
lvim.builtin.terminal.active = true
-- lvim.builtin.nvimtree.setup.view.side = "left"
-- lvim.builtin.nvimtree.setup.renderer.icons.show.git = false

-- ###################################################### --
-- neopyter (jupyter configuration)
-- ###################################################### --
-- -- FIX: Not working
-- on_attach = function(buf)
-- 	local function map(mode, lhs, rhs, desc)
-- 		vim.keymap.set(mode, lhs, rhs, { desc = desc, buffer = buf })
-- 	end
-- 	-- same, recommend the former
-- 	vim.keymap.del("n", "<C-X>")
-- 	map("n", "<C-X>", "<cmd>Neopyter execute notebook:run-cell<cr>", "run selected")
-- 	-- map("n", "<C-Enter>", "<cmd>Neopyter run current<cr>", "run selected")
-- end

-- -- FIX: Not working
-- require("nvim-treesitter.configs").setup({
-- 	textobjects = {
-- 		move = {
-- 			enable = true,
-- 			goto_next_start = {
-- 				["]j"] = "@cellseparator",
-- 				["]c"] = "@cellcontent",
-- 			},
-- 			goto_previous_start = {
-- 				["[j"] = "@cellseparator",
-- 				["[c"] = "@cellcontent",
-- 			},
-- 		},
-- 	},
-- })

-- ###################################################### --
-- persistence
-- ###################################################### --
vim.api.nvim_create_user_command("PeLoadSession", function()
	require("persistence").load()
end, {})

-- Select a session to load
vim.api.nvim_create_user_command("PeSelectSession", function()
	require("persistence").select()
end, {})

-- Load the last session
vim.api.nvim_create_user_command("PeLoadLastSession", function()
	require("persistence").load({ last = true })
end, {})

-- Stop Persistence
vim.api.nvim_create_user_command("PeStopPersistence", function()
	require("persistence").stop()
end, {})
-- ###################################################### --
-- treesitter
-- ###################################################### --

-- treesitter configuration:
-- if you don't want all the parsers change this to a table of the ones you want
lvim.builtin.treesitter.ensure_installed = {
	"bash",
	"c",
	"cpp",
	"dockerfile",
	"diff",
	"javascript",
	"json",
	"julia",
	"lua",
	"make",
	"markdown",
	"python",
	"typescript",
	"tsx",
	"css",
	"rust",
	"java",
	"yaml",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enable = true

-- ###################################################### --
-- generic LSP settings
-- ###################################################### --

-- -- make sure server will always be installed even if the server is in skipped_servers list
lvim.lsp.installer.setup.ensure_installed = {
	"clangd",
	"cmake",
	"pyright",
	"ltex",
	"yamlls",
}
-- -- change UI setting of `LspInstallInfo`
-- -- see <https://github.com/williamboman/nvim-lsp-installer#default-configuration>
-- lvim.lsp.installer.setup.ui.check_outdated_servers_on_open = false
-- lvim.lsp.installer.setup.ui.border = "rounded"
-- lvim.lsp.installer.setup.ui.keymaps = {
--     uninstall_server = "d",
--     toggle_server_expand = "o",
-- }
-- ---@usage disable automatic installation of servers
-- lvim.lsp.installer.setup.automatic_installation = false

-- ---configure a server manually. !!Requires `:LvimCacheReset` to take effect!!
-- ---see the full default list `:lua print(vim.inspect(lvim.lsp.automatic_configuration.skipped_servers))`
-- vim.list_extend(lvim.lsp.automatic_configuration.skipped_servers, { "pyright" })
local opts = {} -- check the lspconfig documentation for a list of all possible options
require("lvim.lsp.manager").setup("pyright", opts)
require("lvim.lsp.manager").setup("ltex", opts)
require("lvim.lsp.manager").setup("yamlls", opts)
require("lvim.lsp.manager").setup("glsl_analyser", opts)
--

-- ###################################################### --
-- DAP configuration
-- ###################################################### --

local clangd_flags = {
	"--offset-encoding=utf-16", --temporary fix for null-ls
	"--experimental-modules-support",
}

local opts = {
	cmd = { "clangd", unpack(clangd_flags) },
	-- init_options = { compileFlags = { "-std=c++20" } },
}
require("lvim.lsp.manager").setup("clangd", opts)

lvim.builtin.dap.on_config_done = function(dap)
	dap.adapters.codelldb = {
		type = "server",
		port = "${port}",
		executable = {
			-- provide the absolute path for `codelldb` command if not using the one installed using `mason.nvim`
			command = "codelldb",
			args = { "--port", "${port}" },

			-- On windows you may have to uncomment this:
			-- detached = false,
		},
	}

	dap.configurations.cpp = {
		{
			name = "Launch file",
			type = "codelldb",
			request = "launch",
			program = function()
				local path
				vim.ui.input(
					{ prompt = "Path to executable: ", default = vim.loop.cwd() .. "/build/Example/" },
					function(input)
						path = input
					end
				)
				vim.cmd([[redraw]])
				return path
			end,
			cwd = "${workspaceFolder}",
			stopOnEntry = false,
		},
	}
	dap.configurations.c = dap.configurations.cpp
end

-- ---remove a server from the skipped list, e.g. eslint, or emmet_ls. !!Requires `:LvimCacheReset` to take effect!!
-- ---`:LvimInfo` lists which server(s) are skipped for the current filetype
-- lvim.lsp.automatic_configuration.skipped_servers = vim.tbl_filter(function(server)
--   return server ~= "emmet_ls"
-- end, lvim.lsp.automatic_configuration.skipped_servers)

-- -- you can set a custom on_attach function that will be used for all the language servers
-- -- See <https://github.com/neovim/nvim-lspconfig#keybindings-and-completion>
-- lvim.lsp.on_attach_callback = function(client, bufnr)
--   local function buf_set_option(...)
--     vim.api.nvim_buf_set_option(bufnr, ...)
--   end
--   --Enable completion triggered by <c-x><c-o>
--   buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")
-- end

-- ###################################################### --
-- formatters config
-- ###################################################### --

local formatters = require("lvim.lsp.null-ls.formatters")
formatters.setup({
	{ command = "csharpier", args = { "tabWidth", "2" } },
	{ command = "blue", filetypes = { "python" } },
	{ command = "stylua", filetypes = { "lua" } },
	{ command = "gersemi", filetypes = { "cmake" } },
	{ command = "yamlfmt", filetypes = { "yml", "yaml" } },
	-- { command = "mdformat", filetypes = { "markdown" } },
	--   { command = "isort", filetypes = { "python" } },
	--   {
	--     -- each formatter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
	--     command = "prettier",
	--     ---@usage arguments to pass to the formatter
	--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
	--     extra_args = { "--print-with", "100" },
	--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
	--     filetypes = { "typescript", "typescriptreact" },
	--   },
})

-- ###################################################### --
-- linters config
-- ###################################################### --

local linters = require("lvim.lsp.null-ls.linters")
linters.setup({
	{
		command = "eslint_d",
		filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact", "vue" },
	},
	-- { command = "flake8", filetypes = { "python" } },
	--   {
	--     -- each linter accepts a list of options identical to https://github.com/jose-elias-alvarez/null-ls.nvim/blob/main/doc/BUILTINS.md#Configuration
	--     command = "shellcheck",
	--     ---@usage arguments to pass to the formatter
	--     -- these cannot contain whitespaces, options such as `--line-width 80` become either `{'--line-width', '80'}` or `{'--line-width=80'}`
	--     extra_args = { "--severity", "warning" },
	--   },
	--   {
	--     command = "codespell",
	--     ---@usage specify which filetypes to enable. By default a providers will attach to all the filetypes it supports.
	--     filetypes = { "javascript", "python" },
	--   },
})

-- Additional Plugins
-- lvim.plugins = {
--     {
--       "folke/trouble.nvim",
--       cmd = "TroubleToggle",
--     },
-- }

-- Autocommands (https://neovim.io/doc/user/autocmd.html)
vim.api.nvim_create_autocmd("BufEnter", {
	pattern = { "*.json", "*.jsonc", "*.md", "*.txt" },
	-- enable wrap mode for json files onlyA
	command = "setlocal wrap",
})
-- vim.api.nvim_create_autocmd("FileType", {
--   pattern = "zsh",
--   callback = function()
--     -- let treesitter use bash highlight for zsh files as well
--     require("nvim-treesitter.highlight").attach(0, "bash")
--   end,
-- })
