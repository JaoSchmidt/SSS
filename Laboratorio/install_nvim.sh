mkdir -p appImages
if curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage; then
	mv nvim.appimage appImages/
	chmod u+x appImages/nvim.appimage
	if [[ ! -z $(grep "alias nvim=$(pwd)/appImages/nvim.appimage" "$HOME/.bashrc") ]]; then 
		echo "alias pronto, digite \"nvim\""
	else
		echo "alias nvim=$(pwd)/appImages/nvim.appimage" >> ~/.bash_aliases
		source ~/.bash_aliases
	fi
	# download vim-plug
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	mkdir -p $HOME/.config/nvim/
	echo 'set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
source ~/.vimrc' > $HOME/.config/nvim/init.vim
	cp $(git rev-parse --show-toplevel)/.vimrc $HOME
fi
