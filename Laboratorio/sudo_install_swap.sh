#!/bin/bash

if [ "$EUID" -ne 0 ]; then
	echo "Execute como root"
	exit
fi


if [ -f "/swapfile" ]; then
	echo "swapfile já existe"
else
	sudo fallocate -l 8G /swapfile
	sudo chmod 600 /swapfile
	sudo mkswap /swapfile
	sudo swapon /swapfile
	echo '/swapfile swap swap default 0 0' >> /etc/fstab
	sudo swapon --show
fi	
