echo -e "${red}Inserindo script de limpeza...${NC}"

echo "#!/bin/sh
rm -R /home/convidado
mkdir /home/convidado

mkdir -p "/home/convidado/Área de Trabalho"
mkdir -p /home/convidado/Documentos
mkdir -p /home/convidado/Downloads
mkdir -p /home/convidado/Música
mkdir -p /home/convidado/Imagens
mkdir -p /home/convidado/Vídeos
mkdir -p /home/convidado/Público
mkdir -p /home/convidado/Modelos


chmod 755 -R /home/convidado
chown -R convidado:convidado /home/convidado" > /root/Limpeza.sh

chmod +x /root/Limpeza.sh

echo "[Unit]
Deion=Run Limpeza for cleaning users

[Service]
Type=oneshot
RemainAfterExit=true
ExecStop=/root/Limpeza.sh

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/limpeza.service

systemctl enable limpeza

sudo useradd convidado
echo "convidado:convidado" | sudo chpasswd